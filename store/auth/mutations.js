export default {
    SET_TOKEN(state, { token }) {
      state.token = token
    },
    SET_EMAIL(state, { email }) {
      state.email = email;
    },
    SET_USER(state,  { user }) {
      state.user = user;
    }
}
