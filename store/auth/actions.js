export default {
    async getUser(context, userId) {
        return await this.$axios.get(`http://localhost:3001/600/users/${userId}`, {
            headers: {
                Authorization: `Bearer ${this.$store.getters.auth('userId')}`
            }
        })
    }
}