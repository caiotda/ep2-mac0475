export default {
    async signUpUser({ state }) {
        const {userName, email, password} = state;
        return await this.$axios.post('http://localhost:3001/register', {
            email,
            password,
            userName
        });
    }
}