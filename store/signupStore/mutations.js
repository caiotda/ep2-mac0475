export default {
    SET_EMAIL(state, { newEmail }) {
        state.email = newEmail;
    },
    SET_PASSWORD(state, { newPassword }) {
        state.password = newPassword;
    },
    SET_USERNAME(state,  { newUsername }) {
        state.username = newUsername;
    }
}
