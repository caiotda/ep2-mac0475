export default {
    async loginUser({ state }) {
        const {email, password} = state;
        return await this.$axios.post('http://localhost:3001/login', {
            email,
            password,
        });
    }
}